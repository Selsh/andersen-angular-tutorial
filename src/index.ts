// h1 TypeScript

// Примитивные типы
const is: boolean = true;
const text: string = 'Lorem ipsum';
const pi: number = 3.14;

let response: any = is;
response = text;
response = pi;

// Массивы
const nameColors: Array<string> = ['red', 'blue'];
const hexColors: string[] = ['#000', '#fff'];

const matrix: number[][] = [
	[1, 2, 3],
	[1, 2, 3],
	[1, 2, 3]
];

// Объекты

const cssClasses: object = {
	'header': true,
	'header_dark': false,
	'header_white': true
};

// Enum

enum Direction {
	top = 'top',
	right = 'right',
	bottom = 'bottom',
	left = 'left'
}

const headerPosition: Direction = Direction.top;


// Interface

interface Point {
	x: number;
	y: number;
}

interface Circle {
	center: Point;
	radius: number;
	border?: number;
	[propName: string]: number | Point;
}

interface zAxis extends Point {
	z: number;
}

const toggleButton: Circle = {
	center: {
		x: 20,
		y: 20
	},
	radius: 10
};

// https://github.com/Microsoft/TypeScript/blob/master/src/lib/dom.generated.d.ts

// Functions
function moveButton(button: Circle, direction: Direction = Direction.right, length?: number): Circle {
	switch (direction) {
		case Direction.top:
			button.center.y - length;
			break;
		case Direction.right:
			button.center.x + length;
			break;
		case Direction.bottom:
		button.center.y + length;
			break;
		case Direction.left:
			button.center.x - length;
			break;
	}
	return button;
}


// Classes

class Unit {
	private _health: number;

	public constructor(health: number = 100) {
		this._health = health;
	}

	public get health(): number {
		return this._health;
	}
}

const custom: Unit = new Unit();

class Player extends Unit {
	private _damage : number;
	
	public constructor(
		health: number,
		damage: number = 10
	) {
		super(health);
		this._damage = damage;		
	}

	public get damage() : number {
		return this._damage;
	}
}


const me: Player = new Player(200, 20);

abstract class BaseRest {
	private domain: string = 'https://jsonplaceholder.typicode.com';

	public abstract getEntityName(): string;

	public getUrl(): string {
		return [this.domain, this.getEntityName()].join('/');
	}

	public fetchAll(): Promise<Array<object>> {
		return new Promise(
				(resolve: Function, reject: Function) => {
					console.log(typeof resolve)
					const xhr = new XMLHttpRequest();

					xhr.open('GET', this.getUrl(), true);

					xhr.onload = () => resolve(xhr.response);
					xhr.onerror = () => reject(xhr.status);

					xhr.send();
				}
			)
			.then((response: string) => JSON.parse(response))
			.catch((error: Error) => console.error(error));
	}
}

class UserService extends BaseRest {
	public getEntityName(): string {
		return 'users';
	}
}

(new UserService())
	.fetchAll()
	.then((data: Array<object>) => console.log(data));




// Decorators

(function() {
	let multiSum = function() {
		return Array.prototype.reduce.call(arguments, (previousValue, currentItem) => { return previousValue + currentItem; }, 0);
	};

	function logDecorator(func) {
		return function() {
			const result = func.apply(this, arguments);
			console.log(`Result: ${ result }`);
			return result;
		};
	}

	multiSum = logDecorator(multiSum);

	multiSum(1, 2, 3, 4, 5);
})();

function ClassDecorator() {
	return (CustomClass: Function) => {
		Object.defineProperty(
			CustomClass.prototype,
			'customProp',
			{ value: 'lorem' }
		);
	};
}

function PropDecorator(target: any, key: string) {
	let _val = this[key];

	var getter = function () {
		return _val.toUpperCase();
	};

	var setter = function (newVal) {
		_val = newVal;
	};

	if (delete this[key]) {
		Object.defineProperty(target, key, {
			get: getter,
      		set: setter,
	      	enumerable: true,
	      	configurable: true
    	});
	}
}

@ClassDecorator()
class User {
	public firstName: string;
	@PropDecorator public lastName: string;
	public coutry?: string;

	public constructor(firstName: string, lastName: string, country?: string) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.coutry = country;
	}
}

console.log(new User('Bill', 'Gates'));
